*** Comments ***
To learn more about these .robot files, see
https://robotframework-jupyterlibrary.readthedocs.io/en/stable/.

*** Settings ***
Documentation     Server Proxies in Notebook
Library           JupyterLibrary
Suite Setup       Start Notebook Tests
Test Setup        Switch Window    MAIN
Test Tags         app:notebook

*** Variables ***
${XP_NEW_MENU}   xpath://jp-toolbar[contains(@class, "jp-FileBrowser-toolbar")]//*[contains(text(), "New")]
${XP_OPEN_COMMAND}   xpath://li[@data-command = "server-proxy:open"]

*** Keywords ***
Start Notebook Tests
    Open Notebook
    Tag With JupyterLab Metadata
    Set Screenshot Directory    ${OUTPUT DIR}${/}notebook

Launch With Toolbar Menu
    [Arguments]    ${title}
    Mouse Over   ${XP_NEW_MENU}
    Click Element    ${XP_NEW_MENU}
    ${item} =   Set Variable   ${XP_OPEN_COMMAND}//div[text() = '${title}']
    Wait Until Element Is Visible   ${item}
    Mouse Over   ${item}
    Click Element   ${item}

*** Test Cases ***
Notebook Loads
    Capture Page Screenshot    00-smoke.png

Launch Browser Tab
    Click Launcher    foo
    Wait Until Keyword Succeeds    3x    0.5s    Switch Window    title:Hello World
    Location Should Contain    foo
    Page Should Contain    Hello World
    Close Window
    [Teardown]    Switch Window    title:Home

Launch Another Browser Tab
    Click Launcher    bar
    Wait Until Keyword Succeeds    3x    0.5s    Switch Window    title:Hello World
    Location Should Contain    bar
    Page Should Contain    Hello World
    Close Window
    [Teardown]    Switch Window    title:Home
