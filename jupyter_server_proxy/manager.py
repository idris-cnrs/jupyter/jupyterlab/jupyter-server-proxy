"""Manager for jupyter server proxy"""

import asyncio
from collections import namedtuple

from jupyter_server.utils import url_path_join as ujoin
from traitlets import Dict, Instance, Int, Unicode
from traitlets.config import LoggingConfigurable

ServerProxy = namedtuple(
    "ServerProxy",
    ["name", "url", "cmd", "port", "managed", "unix_socket"],
    defaults=[""] * 6,
)
ServerProxyUserInput = namedtuple(
    "ServerProxy",
    ["name", "args", "env"],
    defaults=[""] * 3,
)
ServerProxyProc = namedtuple("ServerProxyProc", ["name", "proc"], defaults=[""] * 2)


class ServerProxyAppManager(LoggingConfigurable):
    """
    A class for listing and stopping server proxies that are started
    by jupyter server proxy.
    """

    server_proxy_apps = Dict(
        key_trait=Unicode(),
        value_trait=Instance(ServerProxy),
        help="List of server proxy apps",
    )

    _server_proxy_app_user_input = Dict(
        key_trait=Unicode(),
        value_trait=Instance(ServerProxyUserInput),
        help="List of server proxy app user inputs",
    )

    _server_proxy_procs = Dict(
        key_trait=Unicode(),
        value_trait=Instance(ServerProxyProc),
        help="List of server proxy app proc objects",
    )

    num_active_server_proxy_apps = Int(
        0, help="Total number of currently running proxy apps"
    )

    def add_server_proxy_app(
        self,
        name,
        base_url,
        cmd,
        port,
        unix_socket,
        proc,
    ):
        # Add proxy server metadata
        self.server_proxy_apps[name] = ServerProxy(
            name=name,
            url=ujoin(base_url, name),
            cmd=" ".join(cmd),
            port=port,
            managed=True if proc else False,
            unix_socket=unix_socket,
        )

        # Add proxy server proc object so that we can send SIGTERM
        # when user chooses to shut it down
        self._server_proxy_procs[name] = ServerProxyProc(
            name=name,
            proc=proc,
        )
        self.num_active_server_proxy_apps += 1
        self.log.debug("Server proxy %s added to server proxy manager" % name)

    def post_server_proxy_app_user_input(self, name, args, env):
        """Add server proxy app's user input CLI and env vars to proxy app"""
        # Check if server proxy is already running
        app = self._get_server_proxy_proc(name)
        if app.proc and app.proc.running:
            self.log.warning(
                "Attempting to change user input args/env of already running "
                "server proxy %s. Ignoring..." % name
            )
            return

        # Add new and most recent args to dict
        self._server_proxy_app_user_input[name] = ServerProxyUserInput(
            name=name, args=args, env=env
        )
        self.log.debug(
            "User input args are %s and env is %s for server proxy %s"
            % (args, env, name)
        )

    def get_server_proxy_app_user_input(self, name):
        """Get server proxy app's user input CLI and env vars to proxy app"""
        return self._server_proxy_app_user_input.get(name, ServerProxyUserInput())

    def del_server_proxy_app(self, name):
        """Remove a launched proxy server from list"""
        try:
            del self.server_proxy_apps[name]
            del self._server_proxy_procs[name]
            self.num_active_server_proxy_apps -= 1
        except KeyError:
            self.log.warning(
                "Cannot remove non existing server proxy %s in server "
                "proxy manager" % name
            )

    def get_server_proxy_app(self, name):
        """Get a given server proxy app"""
        return self.server_proxy_apps.get(name, ServerProxy())

    def _get_server_proxy_proc(self, name):
        """Get a given server proxy app"""
        return self._server_proxy_procs.get(name, ServerProxyProc())

    def list_server_proxy_apps(self):
        """List all active server proxy apps"""
        return list(self.server_proxy_apps.values())

    def _list_server_proxy_procs(self):
        """List all active server proxy proc objs"""
        return list(self._server_proxy_procs.values())

    async def terminate_server_proxy_app(self, name):
        """Terminate a server proxy by sending SIGTERM"""
        app = self._get_server_proxy_proc(name)
        try:
            # Here we send SIGTERM signal to terminate proxy app
            # graciously so we can restart it if needed. Note that
            # some servers may not get stopped by sending SIGTERM
            # signal (example is mlflow server). In this case, it is
            # user's responsibility to write wrapper scripts around
            # proxy app's executable to terminate them cleanly using
            # TERM signal. It is also important to set exit code to 0
            # when using such wrappers when proxy apps shutdown.
            await app.proc.terminate()

            # Remove proxy app from list
            self.del_server_proxy_app(name)

            self.log.debug("Server proxy %s removed from server proxy manager" % name)

            return True
        except (KeyError, AttributeError):
            msg = (
                f"Proxy {name} not found. Are you sure the "
                f"{name} is managed by jupyter-server-proxy?"
            )
            self.log.warning(msg)
            raise Exception(msg)
        except Exception as e:
            self.log.warning(
                "Failed to terminate server proxy %s due to %s" % (name, e)
            )
            raise e

    async def terminate_all(self):
        """Close all server proxy and cleanup"""
        for app in self.server_proxy_apps:
            await self.terminate_server_proxy_app(app)

    async def monitor(self, monitor_interval):
        while True:
            procs = self._list_server_proxy_procs()

            # Check if processes are running
            for proc in procs:
                running = proc.proc.running
                if not running:
                    self.log.warning(
                        "Server proxy %s is not running anymore. "
                        "Removing from server proxy manager" % proc.name
                    )
                    self.del_server_proxy_app(proc.name)

            await asyncio.sleep(monitor_interval)
