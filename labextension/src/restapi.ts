import { URLExt } from '@jupyterlab/coreutils';
import { showDialog, Dialog } from '@jupyterlab/apputils';
import { ServerConnection } from '@jupyterlab/services';
import { TranslationBundle } from '@jupyterlab/translation';
import { IModel } from './serverproxy';

/**
 * The url for the server proxy service.
 */
const SERVER_PROXY_SERVICE_URL = 'server-proxy/api/servers';

/**
 * Construct request URL with query parameters.
 *
 * @param baseUrl - BaseURL for the request.
 *
 * @param serverName - Name of the server proxy along with instance
 *
 * @param trans - Translation bundle.
 *
 * @returns Request URL.
 */
function constructRequestURL(
  baseUrl: string,
  serverName: string,
  trans: TranslationBundle
): string {
  const instanceName = serverName.split('/');
  if (Array.isArray(instanceName) && instanceName.length !== 2) {
    const msg = trans.__(`Server proxy "${serverName}" name is not valid`);
    console.warn(msg);
    void showDialog({
      title: trans.__('Warning'),
      body: msg,
      buttons: [Dialog.okButton({ label: 'Dismiss' })]
    });
  }
  const queryArgs = { name: instanceName[0], instance: instanceName[1] };
  const url =
    URLExt.join(baseUrl, SERVER_PROXY_SERVICE_URL) +
    URLExt.objectToQueryString(queryArgs);
  return url;
}

/**
 * List the running server proxy apps.
 *
 * @param settings - The server settings to use.
 *
 * @returns A promise that resolves with the list of running session models.
 */
export async function listRunning(
  settings: ServerConnection.ISettings = ServerConnection.makeSettings()
): Promise<IModel[]> {
  const url = URLExt.join(settings.baseUrl, SERVER_PROXY_SERVICE_URL);
  const response = await ServerConnection.makeRequest(url, {}, settings);
  if (response.status !== 200) {
    const err = await ServerConnection.ResponseError.create(response);
    throw err;
  }
  const data = await response.json();

  if (!Array.isArray(data)) {
    throw new Error('Invalid server proxy list');
  }

  return data;
}

/**
 * Shut down a server proxy app by name.
 *
 * @param serverName - The name of the target server proxy app.
 *
 * @param trans - Translation bundle.
 *
 * @param settings - The server settings to use.
 *
 * @returns A promise that resolves when the app is shut down.
 */
export async function shutdown(
  serverName: string,
  trans: TranslationBundle,
  settings: ServerConnection.ISettings = ServerConnection.makeSettings()
): Promise<void> {
  const url = constructRequestURL(settings.baseUrl, serverName, trans);
  const init = { method: 'DELETE' };
  const response = await ServerConnection.makeRequest(url, init, settings);
  if (response.status === 404) {
    const msg = trans.__(
      `Server proxy "${serverName}" is not running anymore. It will be removed from this list shortly`
    );
    console.warn(msg);
    void showDialog({
      title: trans.__('Warning'),
      body: msg,
      buttons: [Dialog.okButton({ label: 'Dismiss' })]
    });
  } else if (response.status === 403) {
    // This request cannot be made via JupyterLab UI and hence we just throw
    // console log
    const msg = trans.__('Provide a running server proxy name to terminate');
    console.warn(msg);
  } else if (response.status !== 204) {
    const err = await ServerConnection.ResponseError.create(response);
    throw err;
  }
}

/**
 * Setup proxy app args and env variables
 *
 * @param serverName - Name of the server proxy app
 *
 * @param args - CLI args of the server proxy app
 *
 * @param env - Environment variables of the server proxy app
 *
 * @param trans - Translation bundle.
 *
 * @param settings - The server settings to use.
 *
 * @returns A promise that resolves with the list of running session models.
 */
export async function setupServerProxyApp(
  serverName: string,
  args: string,
  env: string,
  trans: TranslationBundle,
  settings: ServerConnection.ISettings = ServerConnection.makeSettings()
): Promise<number> {
  const url = constructRequestURL(settings.baseUrl, serverName, trans);
  // ServerConnection won't automaticy add this header when the body in not none.
  const header = new Headers({ 'Content-Type': 'application/json' });
  const data = JSON.stringify({ args: args, env: env });
  const init = { method: 'POST', headers: header, body: data };
  return ServerConnection.makeRequest(url, init, settings).then(response => {
    if (response.status !== 200) {
      throw new ServerConnection.ResponseError(response);
    }
    return response.status;
  });
}
