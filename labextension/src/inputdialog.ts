import { Widget } from '@lumino/widgets';

/**
 * Widget for inputing proxy app arguments and env variables
 */
export class ServerProxyAppArgsWidget extends Widget {
  constructor(name: string, numInstances: number) {
    super({ node: Private.createOpenNode(name, numInstances) });
  }

  /**
   * Get the value of the widget
   */
  getValue(): [string, string, string] {
    return [
      this.inputNode[0].value,
      this.inputNode[1].value,
      this.selectOption[0].value
    ];
  }

  /**
   * Get the input text node.
   */
  get inputNode(): [HTMLInputElement, HTMLInputElement] {
    return [
      this.node.getElementsByTagName('input')[0] as HTMLInputElement,
      this.node.getElementsByTagName('input')[1] as HTMLInputElement
    ];
  }

  /**
   * Get the select option
   */
  get selectOption(): [HTMLOptionElement] {
    return [
      this.node.getElementsByTagName('select')[0]
        .selectedOptions[0] as HTMLOptionElement
    ];
  }
}

namespace Private {
  export function createOpenNode(
    name: string,
    numInstances: number
  ): HTMLElement {
    const body = document.createElement('div');
    const instanceLabel = document.createElement('label');
    instanceLabel.textContent = 'Select endpoint:';
    const instanceList = document.createElement('select');
    instanceList.setAttribute('id', 'instance_id');
    for (let i = 0; i < numInstances; i++) {
      const option = document.createElement('option');
      option.value = i.toString();
      option.text = `${name}/${i.toString()}`;
      if (i === 0) {
        option.selected = true;
      }
      instanceList.appendChild(option);
    }

    body.appendChild(instanceLabel);
    body.appendChild(instanceList);

    // Chars that can enable pipelining. As we add CLI args to app, it is easy
    // to inject commands by pipelining. Anyways, JupyterLab runs in userland
    // so that should not be a big security hole. Anyways, we forbid users
    // to use these chars in input dialog
    const forbiddenChars = /[&|;]/;

    const argsLabel = document.createElement('label');
    argsLabel.textContent = 'CLI arguments:';

    const args = document.createElement('input');
    args.value = '';
    args.placeholder = '--arg1 value --arg2 value';
    args.addEventListener('keypress', e => {
      if (forbiddenChars.test(e.key)) {
        e.preventDefault();
      }
    });

    body.appendChild(argsLabel);
    body.appendChild(args);

    const envLabel = document.createElement('label');
    envLabel.textContent = 'Environment variables:';

    const env = document.createElement('input');
    env.value = '';
    env.placeholder = 'TOM=CAT JERRY=MOUSE';
    env.addEventListener('keypress', e => {
      if (forbiddenChars.test(e.key)) {
        e.preventDefault();
      }
    });

    body.appendChild(envLabel);
    body.appendChild(env);
    return body;
  }

  export const id = 0;
}
